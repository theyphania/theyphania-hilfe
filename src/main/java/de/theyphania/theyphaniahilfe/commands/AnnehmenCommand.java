package de.theyphania.theyphaniahilfe.commands;

import de.theyphania.theyphaniahilfe.TheyphaniaHilfe;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.PlayerArgument;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import static net.md_5.bungee.api.ChatColor.GRAY;
import static net.md_5.bungee.api.ChatColor.RED;

public class AnnehmenCommand
{
	TheyphaniaHilfe plugin = TheyphaniaHilfe.get();
	
	public AnnehmenCommand()
	{
		this.registerCommands();
		plugin.log("Registering AnnehmenCommand (/annehmen, /annehmen <Player>).");
	}
	
	private void registerCommands()
	{
		String commandName = "annehmen";
		String commandPermission = "theyphania.annehmen";
		new CommandAPICommand(commandName)
			.withArguments(new PlayerArgument("spieler").overrideSuggestions(plugin.getHilfegesuche().keySet()))
			.withPermission(commandPermission)
			.executesPlayer((helper, args) ->
			{
				String helperName = helper.getDisplayName();
				Player target = (Player) args[0];
				String targetName = target.getDisplayName();
				if (!(plugin.getHilfegesuche().containsKey(targetName)))
				{
					helper.sendMessage(RED + " Das angegebene Hilfegesuch existiert nicht oder wurde bereits bearbeitet.");
					return;
				}
				Bukkit.broadcast(GRAY + "Das Hilfegesuch für " + targetName + " wurde von " + helperName + " angenommen.", "theyphania.annehmen");
				target.sendMessage(GRAY + "Dein Hilfegesuch wurde von " + helperName + " angenommen.");
				plugin.getHilfegesuche().remove(targetName);
			}).register();
		new CommandAPICommand(commandName)
			.withPermission(commandPermission)
			.executes((sender, args) ->
			{
				sender.sendMessage(GRAY + " Dieser Befehl nimmt das Hilfegesuch des angegebenen Spielers an.");
			}).register();
	}
}
