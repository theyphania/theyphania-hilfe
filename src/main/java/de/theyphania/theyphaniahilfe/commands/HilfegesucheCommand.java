package de.theyphania.theyphaniahilfe.commands;

import de.theyphania.theyphaniahilfe.TheyphaniaHilfe;
import dev.jorel.commandapi.CommandAPICommand;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.Map.Entry;

import static net.md_5.bungee.api.ChatColor.BOLD;
import static net.md_5.bungee.api.ChatColor.GRAY;
import static net.md_5.bungee.api.ChatColor.GREEN;

public class HilfegesucheCommand
{
	TheyphaniaHilfe plugin = TheyphaniaHilfe.get();
	
	public HilfegesucheCommand()
	{
		this.registerCommands();
		plugin.log("Registering HilfegesucheCommand (/hilfegesuche).");
	}
	
	private void registerCommands()
	{
		new CommandAPICommand("hilfegesuche")
			.withPermission("theyphania.annehmen")
			.executesPlayer((player, args) ->
			{
				TextComponent startMessage = new TextComponent(GRAY + "Die folgenden Spieler benötigen noch Hilfe:");
				player.sendMessage(startMessage);
				for (Entry<String, Long> e : plugin.getHilfegesuche().entrySet())
				{
					String targetName = e.getKey();
					int timeSinceInMinutes = (int) ((System.currentTimeMillis() - e.getValue()) / 60_000);
					TextComponent msg = new TextComponent(GRAY + targetName + " (vor " + timeSinceInMinutes + " Minuten)");
					ClickEvent clickEvent = new ClickEvent(Action.RUN_COMMAND, "/annehmen " + targetName);
					TextComponent msg2 = new TextComponent(GREEN + "" + BOLD + "[Annehmen]");
					msg2.setClickEvent(clickEvent);
					msg.addExtra(msg2);
					player.sendMessage(msg);
				}
			}).register();
	}
}
