package de.theyphania.theyphaniahilfe.commands;

import de.theyphania.theyphaniahilfe.TheyphaniaHilfe;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.CommandPermission;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import static net.md_5.bungee.api.ChatColor.BOLD;
import static net.md_5.bungee.api.ChatColor.GRAY;
import static net.md_5.bungee.api.ChatColor.GREEN;
import static net.md_5.bungee.api.ChatColor.RED;

public class HilfeCommand
{
	TheyphaniaHilfe plugin = TheyphaniaHilfe.get();
	int cooldownInMinutes = 5;
	
	public HilfeCommand()
	{
		this.registerCommands();
		plugin.log("Registering HilfeCommand (/hilfe).");
	}
	
	private void registerCommands()
	{
		new CommandAPICommand("hilfe")
			.withPermission(CommandPermission.NONE)
			.executesPlayer((player, args) ->
			{
				if (player.hasPermission("theyphania.annehmen"))
				{
					player.performCommand("hilfegesuche");
					return;
				}
				String playerName = player.getDisplayName();
				long cooldownInMillis = cooldownInMinutes * 60 * 1_000;
				if (plugin.getHilfegesuche().containsKey(playerName) && (System.currentTimeMillis() - plugin.getTimeFromHilfegesuch(playerName)) <= cooldownInMillis)
				{
					player.sendMessage(RED + " Du hast bereits ein Hilfegesuch gestellt, bitte warte noch ein wenig.");
					return;
				}
				
				player.sendMessage(GRAY + "Dein Hilfegesuch wurde gesendet!");
				plugin.addHilfegesuch(playerName, System.currentTimeMillis());
				for (Player p : Bukkit.getServer().getOnlinePlayers())
				{
					if (!p.hasPermission("theyphania.annehmen")) continue;
					TextComponent msg = new TextComponent(GRAY + playerName + " braucht Hilfe. ");
					TextComponent msg2 = new TextComponent(GREEN + "" + BOLD + "[Annehmen]");
					ClickEvent clickEvent = new ClickEvent(Action.RUN_COMMAND, "/annehmen " + playerName);
					msg2.setClickEvent(clickEvent);
					msg.addExtra(msg2);
					p.sendMessage(msg);
				}
			}).register();
	}
}
