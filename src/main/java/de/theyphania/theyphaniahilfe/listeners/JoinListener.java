package de.theyphania.theyphaniahilfe.listeners;

import de.theyphania.theyphaniahilfe.TheyphaniaHilfe;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinListener implements Listener
{
	private final TheyphaniaHilfe plugin = TheyphaniaHilfe.get();
	
	public JoinListener()
	{
		Bukkit.getPluginManager().registerEvents(this, plugin);
		plugin.log("Registering JoinListener (PlayerJoinEvent, PlayerQuitEvent).");
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();
		if (plugin.getHilfegesuche().isEmpty() || !player.hasPermission("theyphania.annehmen")) return;
		Bukkit.getScheduler().runTaskLater(plugin, () -> player.performCommand("hilfegesuche"), 20L);
		
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		plugin.removeHilfegesuch(e.getPlayer().getDisplayName());
	}
}
