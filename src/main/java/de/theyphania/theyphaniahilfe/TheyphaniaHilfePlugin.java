package de.theyphania.theyphaniahilfe;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.md_5.bungee.api.ChatColor.DARK_GRAY;
import static net.md_5.bungee.api.ChatColor.GOLD;
import static net.md_5.bungee.api.ChatColor.GRAY;

public abstract class TheyphaniaHilfePlugin extends JavaPlugin
{
	private String logPrefixColored;
	private String logPrefixPlain;
	private long enableTime;
	
	@Override
	public void onLoad()
	{
		onLoadPre();
		onLoadInner();
		onLoadPost();
	}
	
	private void onLoadPre()
	{
		logPrefixColored = DARK_GRAY + "[" + GOLD + getDescription().getPrefix() + DARK_GRAY + "]" + GRAY;
		logPrefixPlain = ChatColor.stripColor(logPrefixColored);
	}
	
	private void onLoadInner()
	{
	}
	
	private void onLoadPost()
	{
	}
	
	public long getEnableTime()
	{
		return enableTime;
	}
	
	@Override
	public void onEnable()
	{
		if (!onEnablePre()) return;
		onEnableInner();
		onEnablePost();
	}
	
	private boolean onEnablePre()
	{
		enableTime = System.currentTimeMillis();
		
		log("// ========== STARTING PLUGIN ========== //");
		
		return true;
	}
	
	public void onEnableInner()
	{
	}
	
	private void onEnablePost()
	{
		long ms = System.currentTimeMillis() - enableTime;
		log("// ========== DONE (Time: " + ms + "ms) ========== //");
	}
	
	@Override
	public void onDisable()
	{
	}
	
	public void suicide()
	{
		log(Level.WARNING, "Plugin will be killed!");
		Bukkit.getPluginManager().disablePlugin(this);
	}
	
	public void debug(String debugString)
	{
		if (getConfig().getBoolean("debug"))
		{
			log("DEBUG", debugString);
		}
	}
	
	public void error(Exception e)
	{
		log(Level.SEVERE, "Looks like we have a " + e.toString() + " here.");
		log(Level.SEVERE, "Surely it is Janto's fault but here is a bit more detail:");
		e.printStackTrace();
	}
	
	public void log(String prefix, String msg)
	{
		log(Level.INFO, "[" + prefix + "] " + msg);
	}
	
	public void log(String msg)
	{
		log(Level.INFO, msg);
	}
	
	public void log(Level level, String msg)
	{
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (level == Level.INFO)
		{
			console.sendMessage(logPrefixColored + " " + msg);
		}
		else
		{
			Logger.getLogger("Minecraft").log(level, logPrefixPlain + " " + msg);
		}
	}
}
