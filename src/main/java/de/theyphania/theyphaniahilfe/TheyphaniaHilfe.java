package de.theyphania.theyphaniahilfe;

import de.theyphania.theyphaniahilfe.commands.AnnehmenCommand;
import de.theyphania.theyphaniahilfe.commands.HilfeCommand;
import de.theyphania.theyphaniahilfe.commands.HilfegesucheCommand;
import de.theyphania.theyphaniahilfe.listeners.JoinListener;

import java.util.LinkedHashMap;

public class TheyphaniaHilfe extends TheyphaniaHilfePlugin
{
	private final LinkedHashMap<String, Long> hilfegesuche = new LinkedHashMap<>();
	
	private static TheyphaniaHilfe i;
	
	public TheyphaniaHilfe()
	{
		i = this;
	}
	
	public static TheyphaniaHilfe get()
	{
		return i;
	}
	
	@Override
	public void onEnableInner()
	{
		log("You seem tough, but here's a safety Shovel anyways.");
		log("Registering Listeners...");
		new JoinListener();
		log("All Listeners registered successfully.");
		log("Registering Commands...");
		new HilfeCommand();
		new AnnehmenCommand();
		new HilfegesucheCommand();
		log("All Commands registered successfully.");
	}
	
	@Override
	public void onDisable()
	{
		hilfegesuche.clear();
	}
	
	public void addHilfegesuch(String player, long time)
	{
		hilfegesuche.put(player, time);
	}
	
	public Long getTimeFromHilfegesuch(String player)
	{
		return hilfegesuche.get(player);
	}
	
	public LinkedHashMap<String, Long> getHilfegesuche()
	{
		return hilfegesuche;
	}
	
	public void removeHilfegesuch(String player)
	{
		hilfegesuche.remove(player);
	}
}
